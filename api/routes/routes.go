package routes

import (
	"manager/api/handlers"

	"github.com/gorilla/mux"
)

func NewRouter() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/send", handlers.Receive).Methods("POST")
	r.HandleFunc("/consume", handlers.Send).Methods("POST")
	r.HandleFunc("/queue", handlers.Show).Methods("GET")
	r.HandleFunc("/confirm", handlers.Confirm).Methods("POST")
	r.HandleFunc("/refresh", handlers.Refresh).Methods("GET")

	return r
}
