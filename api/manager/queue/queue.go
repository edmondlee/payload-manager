package queue

import (
	"manager/pkg/models"
	"time"
)

var Queue = make(map[string]map[string]map[string]models.Payload)

func TopicExists(topic string) bool {
	_, present := Queue[topic]
	return present
}

func StatusExists(topic, status string) bool {
	_, present := Queue[topic][status]
	return present
}

func AddToQueue(receivedPayload models.Payload) {
	if !TopicExists(receivedPayload.Topic) {
		Queue[receivedPayload.Topic] = make(map[string]map[string]models.Payload)
	}
	if !StatusExists(receivedPayload.Topic, receivedPayload.Status) {
		Queue[receivedPayload.Topic][receivedPayload.Status] = make(map[string]models.Payload)
	}
	Queue[receivedPayload.Topic][receivedPayload.Status][receivedPayload.ID] = receivedPayload
}

func UpdatePayloadStatus(oldStatus string, newStatus string, payload models.Payload) {
	payload.Status = newStatus
	if !StatusExists(payload.Topic, newStatus) {
		Queue[payload.Topic][newStatus] = make(map[string]models.Payload)
	}
	Queue[payload.Topic][newStatus][payload.ID] = payload
	delete(Queue[payload.Topic][oldStatus], payload.ID)
}

func GetPendingPayload(topic string) (models.Payload, string) {
	newStatus := "consumed"
	for _, v := range Queue[topic]["pending"] {
		UpdatePayloadStatus(v.Status, newStatus, v)
		return v, newStatus
	}
	return models.Payload{}, ""
}

func RetryExpiredPayloads() {
	maxRetries := 5
	for k, v := range Queue {
		if StatusExists(k, "consumed") {
			for _, payload := range v["consumed"] {
				timeDiff := time.Now().Sub(payload.DateCreated)
				if timeDiff > time.Duration(24)*time.Hour && payload.Retries >= maxRetries {
					expiredPayload := payload
					UpdatePayloadStatus("consumed", "failed", expiredPayload)
				} else if timeDiff > time.Duration(24)*time.Hour {
					expiredPayload := payload
					expiredPayload.Retries++
					expiredPayload.DateRetried = time.Now()
					UpdatePayloadStatus("consumed", "pending", expiredPayload)
				}
			}
		}
	}
}

func CronRetryExpiredPayloads(seconds time.Duration) {
	RetryExpiredPayloads()
	time.Sleep(seconds * time.Second)
	CronRetryExpiredPayloads(seconds)
}

// Test Helper Function

func QueueTestPayload(topic, status, id string) {
	Queue = make(map[string]map[string]map[string]models.Payload)
	testTime, _ := time.Parse(time.RFC3339, "2019-12-22T21:38:35.392078-08:00")
	testPayload := models.Payload{
		ID:          id,
		Topic:       topic,
		Data:        "test-data",
		Status:      status,
		Retries:     5,
		DateCreated: testTime,
		DateRetried: testTime,
	}
	AddToQueue(testPayload)
}
