package consumer

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"manager/api/manager/queue"
	"manager/pkg/models"
	"net/http"
)

func Consume(topic string) (models.Payload, string, error) {
	_, present := queue.Queue[topic]
	if present != false && len(queue.Queue[topic]) != 0 {
		selectedPayload, status := queue.GetPendingPayload(topic)
		return selectedPayload, status, nil
	} else {
		return models.Payload{}, "", fmt.Errorf("topic %s not in use", topic)
	}
}

func Confirm(topic string, id string, status string) error {
	_, present := queue.Queue[topic][status][id]
	if present != false {
		queue.UpdatePayloadStatus(status, "processed", queue.Queue[topic][status][id])
	} else {
		return fmt.Errorf("cannot confirm invalid payload for topic %s - payload with %s id not found", topic, id)
	}
	return nil
}

//Export Package Functions

func ConsumePayload(topic string, url string) ([]byte, error) {
	r, err := http.Post(fmt.Sprintf("%v/consume", url), "application/json", bytes.NewBuffer([]byte(topic)))
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(r.Body)
	return body, nil
}

func ConfirmPayload(topic, url, id, status string) error {
	var receivedPayload models.Payload
	receivedPayload.ID = id
	receivedPayload.Topic = topic
	receivedPayload.Status = status
	receivedPayloadByte, err := json.Marshal(receivedPayload)
	if err != nil {
		return err
	}
	_, err = http.Post(fmt.Sprintf("%v/confirm", url), "application/json", bytes.NewBuffer([]byte(receivedPayloadByte)))
	if err != nil {
		return err
	}
	return nil
}

func AddPayload(topic, url string, data interface{}) error {
	var receivedPayload models.Payload
	receivedPayload.Topic = topic
	receivedPayload.Data = data
	receivedPayloadByte, err := json.Marshal(receivedPayload)
	if err != nil {
		return err
	}
	_, err = http.Post(fmt.Sprintf("%v/send", url), "application/json", bytes.NewBuffer([]byte(receivedPayloadByte)))
	if err != nil {
		return err
	}
	return nil
}
