package consumer

import (
	"encoding/json"
	"fmt"
	"manager/api/manager/queue"
	"manager/pkg/models"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConsume(t *testing.T) {
	queue.QueueTestPayload("test-topic", "pending", "X-TEST-123")
	actualPayload, actualStatus, err := Consume("test-topic")
	if err != nil {
		t.Fatal(err)
	}
	actualPayloadByte, _ := json.Marshal(actualPayload)
	expectedPayloadString := `{"id":"X-TEST-123","topic":"test-topic","data":"test-data","status":"pending","retries":5,"datecreated":"2019-12-22T21:38:35.392078-08:00","dateretried":"2019-12-22T21:38:35.392078-08:00"}`
	expectedStatus := "consumed"
	assert.Equal(t, expectedPayloadString, string(actualPayloadByte))
	assert.Equal(t, expectedStatus, actualStatus)
}

func TestConsumeError(t *testing.T) {
	queue.Queue = make(map[string]map[string]map[string]models.Payload)
	_, _, err := Consume("test-topic")
	if err == nil {
		t.Fatal("did not produce expected error")
	}
	expectedErr := fmt.Errorf("topic test-topic not in use")
	assert.Equal(t, expectedErr, err)
}

func TestConfirm(t *testing.T) {
	queue.QueueTestPayload("test-topic", "consumed", "X-TEST-123")
	err := Confirm("test-topic", "X-TEST-123", "consumed")
	if err != nil {
		t.Fatal(err)
	}
	queueByte, _ := json.Marshal(queue.Queue)
	actualQueue := string(queueByte)
	expectedQueue := `{"test-topic":{"consumed":{},"processed":{"X-TEST-123":{"id":"X-TEST-123","topic":"test-topic","data":"test-data","status":"processed","retries":5,"datecreated":"2019-12-22T21:38:35.392078-08:00","dateretried":"2019-12-22T21:38:35.392078-08:00"}}}}`
	assert.Equal(t, expectedQueue, actualQueue)
}

func TestConfirmError(t *testing.T) {
	err := Confirm("test-topic", "X-TEST-123", "consumed")
	if err == nil {
		t.Fatal("did not produce expected error")
	}
	expectedErr := fmt.Errorf("cannot confirm invalid payload for topic test-topic - payload with X-TEST-123 id not found")
	assert.Equal(t, expectedErr, err)
}
