package handlers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"manager/api/manager/consumer"
	"manager/api/manager/queue"
	"manager/pkg/models"
	"net/http"
)

func handlerError(w http.ResponseWriter, handler string, err error) {
	log.Printf("%v - error: %v", handler, err)
	json.NewEncoder(w).Encode(fmt.Sprintf(`{error: '%v'}`, err))
}

func Receive(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	var receivedPayload models.Payload
	err := json.Unmarshal(body, &receivedPayload)
	if err != nil {
		handlerError(w, "Receive", err)
		return
	}
	if receivedPayload.Topic == "" {
		handlerError(w, "Receive", fmt.Errorf("cannot add invalid payload to queue - missing topic"))
	} else {
		queue.AddToQueue(receivedPayload)
		json.NewEncoder(w).Encode(queue.Queue[receivedPayload.Topic])
	}
}

func Send(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	topic := string(body)
	selectedPayload, status, err := consumer.Consume(topic)
	if err != nil {
		handlerError(w, "Send", err)
		return
	}
	if selectedPayload.ID == "" {
		handlerError(w, "Send", fmt.Errorf("no pending messages for topic %s", topic))
	} else {
		json.NewEncoder(w).Encode(queue.Queue[topic][status][selectedPayload.ID])
	}
}

func Confirm(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	var receivedPayload models.Payload
	err := json.Unmarshal(body, &receivedPayload)
	if err != nil {
		handlerError(w, "Confirm", err)
	}
	if receivedPayload.Topic == "" {
		handlerError(w, "Confirm", fmt.Errorf("cannot confirm invalid payload for topic %s - missing topic", receivedPayload.Topic))
		return
	} else if receivedPayload.ID == "" {
		handlerError(w, "Confirm", fmt.Errorf("cannot confirm invalid payload for topic %s - missing id", receivedPayload.Topic))
		return
	}
	err = consumer.Confirm(receivedPayload.Topic, receivedPayload.ID, receivedPayload.Status)
	if err != nil {
		handlerError(w, "Confirm", err)
		return
	}
	json.NewEncoder(w).Encode(queue.Queue[receivedPayload.Topic]["processed"][receivedPayload.ID])
}

func Show(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(queue.Queue)
}

func Refresh(w http.ResponseWriter, r *http.Request) {
	queue.RetryExpiredPayloads()
}
