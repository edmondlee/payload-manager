package handlers

import (
	"encoding/json"
	"fmt"
	"manager/api/manager/queue"
	"manager/pkg/models"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReceive(t *testing.T) {
	req, err := http.NewRequest("POST", "/send", strings.NewReader(`{"topic": "test-topic", "data": "test-data"}`))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Receive)
	handler.ServeHTTP(rr, req)
	res := []byte(rr.Body.String())
	var queue map[string]map[string]models.Payload
	err = json.Unmarshal(res, &queue)
	var id string
	for k := range queue["pending"] {
		id = k
	}
	actualPayload := queue["pending"][id]
	expectedPayload := models.Payload{
		ID:          actualPayload.ID,
		Topic:       "test-topic",
		Data:        "test-data",
		Status:      "pending",
		Retries:     0,
		DateCreated: actualPayload.DateCreated,
		DateRetried: actualPayload.DateRetried,
	}
	assert.Equal(t, expectedPayload, actualPayload)
}

func TestReceiveError(t *testing.T) {
	req, err := http.NewRequest("POST", "/send", strings.NewReader(`{"topic": "", "data": "test-data"}`))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Receive)
	handler.ServeHTTP(rr, req)
	actualResp := rr.Body.String()
	expectedResp := fmt.Sprintf("%v%v", `"{error: 'cannot add invalid payload to queue - missing topic'}"`, "\n")
	assert.Equal(t, expectedResp, actualResp)
}

func TestSend(t *testing.T) {
	queue.QueueTestPayload("test-topic", "pending", "X-TEST-123")
	req, err := http.NewRequest("POST", "/consume", strings.NewReader(`test-topic`))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Send)
	handler.ServeHTTP(rr, req)
	actualPayload := rr.Body.String()
	expectedPayload := fmt.Sprintf("%v%v", `{"id":"X-TEST-123","topic":"test-topic","data":"test-data","status":"consumed","retries":5,"datecreated":"2019-12-22T21:38:35.392078-08:00","dateretried":"2019-12-22T21:38:35.392078-08:00"}`, "\n")
	assert.Equal(t, expectedPayload, actualPayload)
}

func TestSendError(t *testing.T) {
	queue.QueueTestPayload("test-topic", "pending", "")
	req, err := http.NewRequest("POST", "/consume", strings.NewReader(`test-topic`))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Send)
	handler.ServeHTTP(rr, req)
	actualResp := rr.Body.String()
	expectedResp := fmt.Sprintf("%v%v", `"{error: 'no pending messages for topic test-topic'}"`, "\n")
	assert.Equal(t, expectedResp, actualResp)
}

func TestConfirm(t *testing.T) {
	queue.QueueTestPayload("test-topic", "pending", "X-TEST-123")
	req, err := http.NewRequest("POST", "/confirm", strings.NewReader(`{"id":"X-TEST-123", "topic":"test-topic"}`))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Confirm)
	handler.ServeHTTP(rr, req)
	actualPayload := rr.Body.String()
	expectedPayload := fmt.Sprintf("%v%v", `{"id":"X-TEST-123","topic":"test-topic","data":"test-data","status":"processed","retries":5,"datecreated":"2019-12-22T21:38:35.392078-08:00","dateretried":"2019-12-22T21:38:35.392078-08:00"}`, "\n")
	assert.Equal(t, expectedPayload, actualPayload)
}

func TestConfirmTopicError(t *testing.T) {
	queue.QueueTestPayload("test-topic", "pending", "X-TEST-123")
	req, err := http.NewRequest("POST", "/confirm", strings.NewReader(`{"id":"X-TEST-123", "topic":""}`))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Confirm)
	handler.ServeHTTP(rr, req)
	actualResp := rr.Body.String()
	expectedResp := fmt.Sprintf("%v%v", `"{error: 'cannot confirm invalid payload for topic  - missing topic'}"`, "\n")
	assert.Equal(t, expectedResp, actualResp)
}

func TestConfirmIDError(t *testing.T) {
	queue.QueueTestPayload("test-topic", "pending", "X-TEST-123")
	req, err := http.NewRequest("POST", "/confirm", strings.NewReader(`{"id":"", "topic":"test-topic"}`))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Confirm)
	handler.ServeHTTP(rr, req)
	actualResp := rr.Body.String()
	expectedResp := fmt.Sprintf("%v%v", `"{error: 'cannot confirm invalid payload for topic test-topic - missing id'}"`, "\n")
	assert.Equal(t, expectedResp, actualResp)
}

func TestConfirmError(t *testing.T) {
	queue.Queue = make(map[string]map[string]map[string]models.Payload)
	req, err := http.NewRequest("POST", "/confirm", strings.NewReader(`{"id":"X-TEST-123", "topic":"test-topic"}`))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Confirm)
	handler.ServeHTTP(rr, req)
	actualResp := rr.Body.String()
	expectedResp := fmt.Sprintf("%v%v", `"{error: 'cannot confirm invalid payload for topic test-topic - payload with X-TEST-123 id not found'}"`, "\n")
	assert.Equal(t, expectedResp, actualResp)
}

func TestShow(t *testing.T) {
	queue.QueueTestPayload("test-topic", "pending", "X-TEST-123")
	queueByte, _ := json.Marshal(queue.Queue)
	actualQueue := string(queueByte)
	expectedQueue := `{"test-topic":{"pending":{"X-TEST-123":{"id":"X-TEST-123","topic":"test-topic","data":"test-data","status":"pending","retries":5,"datecreated":"2019-12-22T21:38:35.392078-08:00","dateretried":"2019-12-22T21:38:35.392078-08:00"}}}}`
	assert.Equal(t, expectedQueue, actualQueue)
}

func TestRefresh(t *testing.T) {
	queue.QueueTestPayload("test-topic", "consumed", "X-TEST-123")
	req, err := http.NewRequest("POST", "/confirm", strings.NewReader(`{"id":"X-TEST-123", "topic":"test-topic"}`))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Refresh)
	handler.ServeHTTP(rr, req)
	queueByte, _ := json.Marshal(queue.Queue)
	actualQueue := string(queueByte)
	expectedQueue := `{"test-topic":{"consumed":{},"failed":{"X-TEST-123":{"id":"X-TEST-123","topic":"test-topic","data":"test-data","status":"failed","retries":5,"datecreated":"2019-12-22T21:38:35.392078-08:00","dateretried":"2019-12-22T21:38:35.392078-08:00"}}}}`
	assert.Equal(t, expectedQueue, actualQueue)
}
