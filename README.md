# Payload-Manager

A message queue system allowing users to:

    - add messages to a categorized queue
    - consume messages from the queue
    - confirm that the consumed messages were successfully processed

## Install

```
go get -u gitlab.com/EdmondLee/payload-manager/api/manager/consumer
```

```
import 	"gitlab.com/EdmondLee/payload-manager/api/manager/consumer"
```

    - $ brew install dep
    - $ brew upgrade dep
    - $ dep ensure -v
    - $ go run main.go

## Statuses

    - "pending"   - payload is waiting to be consumed
    - "consumed"  - payload has been consumed and is waiting for confirmation of successful processing
    - "processed" - the successful processing of they payload has been confirmed

## End Points

### `GET /queue`
Send a request to Payload-Manager to get the current queue. 
    
    INPUT:
    GET http://localhost:8000/queue

    QUEUE:
    {
        "example": {
            "consumed": {},
            "pending": {
                "535AE3E8-5A42-4A41-BAF3-A45B4D9D9BB5": {
                    "id": "535AE3E8-5A42-4A41-BAF3-A45B4D9D9BB5",
                    "topic": "example",
                    "data": {
                        "data": "Mutaichia estabou howoauq ista chitecto beamorga euiaoq icabo.",
                        "id": 135
                    },
                    "status": "pending",
                    "retries": 0,
                    "datecreated": "2019-12-25T16:58:31.31322-08:00",
                    "dateretried": "2019-12-25T16:58:31.313221-08:00"
                }
            },
            "processed": {
                "EBE7DF7F-F40D-4E5B-8B7B-17C86258B761": {
                    "id": "EBE7DF7F-F40D-4E5B-8B7B-17C86258B761",
                    "topic": "example",
                    "data": {
                        "data": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
                        "id": 123
                    },
                    "status": "processed",
                    "retries": 0,
                    "datecreated": "2019-12-25T16:41:14.543567-08:00",
                    "dateretried": "2019-12-25T16:41:14.543567-08:00"
                }
            }
        }
    }

### `GET /refresh`
Send a request to Payload-Manager to refresh the queue. If the queue has a payload with a status of "consumed" and is older than a day old, the status will be changed to "pending" and the number of retries will increase by 1. 

If the retries are greater than 5, the status will change to "failed". 
    
    INPUT:
    GET http://localhost:8000/refresh

    QUEUE:
    {
        "example": {
            "consumed": {},
            "failed": {
                "535AE3E8-5A42-4A41-BAF3-A45B4D9D9BB5": {
                    "id": "535AE3E8-5A42-4A41-BAF3-A45B4D9D9BB5",
                    "topic": "example",
                    "data": {
                        "data": "Mutaichia estabou howoauq ista chitecto beamorga euiaoq icabo.",
                        "id": 135
                    },
                    "status": "failed",
                    "retries": 5,
                    "datecreated": "2019-12-25T16:58:31.31322-08:00",
                    "dateretried": "2019-12-30T06:12:23.21231-08:00"
                }
            },
            "pending": {},
            "processed": {
                "EBE7DF7F-F40D-4E5B-8B7B-17C86258B761": {
                    "id": "EBE7DF7F-F40D-4E5B-8B7B-17C86258B761",
                    "topic": "example",
                    "data": {
                        "data": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
                        "id": 123
                    },
                    "status": "processed",
                    "retries": 0,
                    "datecreated": "2019-12-25T16:41:14.543567-08:00",
                    "dateretried": "2019-12-25T16:41:14.543567-08:00"
                }
            }
        }
    }

### `POST /send`
Send a message to Payload-Manager, adding the message payload the the queue under the specificed topic name. The status of this payload will be set to "pending". 
    
    INPUT:
    POST http://localhost:8000/send

    {
        "topic": "example",
        "data": {
            "data": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.",
            "id": 123
        }
    }

    QUEUE:
    {
        "example": {
            "pending": {
                "EBE7DF7F-F40D-4E5B-8B7B-17C86258B761": {
                    "id": "EBE7DF7F-F40D-4E5B-8B7B-17C86258B761",
                    "topic": "example",
                    "data": {
                        "data": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
                        "id": 123
                    },
                    "status": "pending",
                    "retries": 0,
                    "datecreated": "2019-12-25T16:41:14.543567-08:00",
                    "dateretried": "2019-12-25T16:41:14.543567-08:00"
                }
            }
        }
    }

### `POST /consume`
Send a raw topic name (without quotations) to Payload-Manager to request a payload from that queue. The status of this payload will bet set to "consumed". 
    
    INPUT:
    POST http://localhost:8000/consume

    example

    QUEUE:
    {
        "example": {
            "consumed": {
                "EBE7DF7F-F40D-4E5B-8B7B-17C86258B761": {
                    "id": "EBE7DF7F-F40D-4E5B-8B7B-17C86258B761",
                    "topic": "example",
                    "data": {
                        "data": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
                        "id": 123
                    },
                    "status": "consumed",
                    "retries": 0,
                    "datecreated": "2019-12-25T16:41:14.543567-08:00",
                    "dateretried": "2019-12-25T16:41:14.543567-08:00"
                }
            },
            "pending": {}
        }
    }

### `POST /confirm`
Send a consumed payload back to Payload-Manager after processing to confirm and track its success. If the payload is not confirmed within a day, it will be retried in the pending queue. When the payload has been retried 5 times, the status will change to "failed".
    
    INPUT:
    POST http://localhost:8000/confirm

    {
        "id": "EBE7DF7F-F40D-4E5B-8B7B-17C86258B761",
        "topic": "example",
        "data": {
            "data": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
            "id": 123
        },
        "status": "consumed",
        "retries": 0,
        "datecreated": "2019-12-25T16:41:14.543567-08:00",
        "dateretried": "2019-12-25T16:41:14.543567-08:00"
    }

    QUEUE:
    {
        "example": {
            "consumed": {},
            "pending": {
                "535AE3E8-5A42-4A41-BAF3-A45B4D9D9BB5": {
                    "id": "535AE3E8-5A42-4A41-BAF3-A45B4D9D9BB5",
                    "topic": "example",
                    "data": {
                        "data": "Mutaichia estabou howoauq ista chitecto beamorga euiaoq icabo.",
                        "id": 135
                    },
                    "status": "pending",
                    "retries": 0,
                    "datecreated": "2019-12-25T16:58:31.31322-08:00",
                    "dateretried": "2019-12-25T16:58:31.313221-08:00"
                }
            },
            "processed": {
                "EBE7DF7F-F40D-4E5B-8B7B-17C86258B761": {
                    "id": "EBE7DF7F-F40D-4E5B-8B7B-17C86258B761",
                    "topic": "example",
                    "data": {
                        "data": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
                        "id": 123
                    },
                    "status": "processed",
                    "retries": 0,
                    "datecreated": "2019-12-25T16:41:14.543567-08:00",
                    "dateretried": "2019-12-25T16:41:14.543567-08:00"
                }
            }
        }
    }

## Examples

```
import 	"gitlab.com/EdmondLee/payload-manager/api/manager/consumer"
```

### AddPayload()
Adds a payload to the specified topic. 

    func AddPayload(topic, url string, data interface{}) error

	err := consumer.AddPayload("example", "http://localhost:8000", "{"data": "Tis de data du example.", "id": 135}")
	if err != nil {
		fmt.Println(err)
	}

### ConsumePayload()
Consumes a payload from the specified topic. 
    
    func ConsumePayload(topic string, url string) ([]byte, error) 

    payload, err := consumer.ConsumePayload("example", "http://localhost:8000")
	if err != nil {
		fmt.Println(err)
	}

### ConfirmPayload()
Confirms the successful processing of a payload from the specified topic. 

    func ConfirmPayload(topic, url, id, status string) error 

    err := consumer.ConfirmPayload("example", "http://localhost:8000", "ID-12345-Z", "consumed")

