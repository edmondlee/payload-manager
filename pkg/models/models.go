package models

import (
	"encoding/json"
	"fmt"
	"os/exec"
	"strings"
	"time"
)

type Payload struct {
	ID          string      `json:"id"`
	Topic       string      `json:"topic"`
	Data        interface{} `json:"data"`
	Status      string      `json:"status"`
	Retries     int         `json:"retries"`
	DateCreated time.Time   `json:"datecreated"`
	DateRetried time.Time   `json:"dateretried"`
}

func (r *Payload) UnmarshalJSON(bytes []byte) error {
	var receivedPayload map[string]interface{}
	err := json.Unmarshal(bytes, &receivedPayload)
	if err != nil {
		return err
	}
	if receivedPayload["topic"] != nil {
		r.Topic = fmt.Sprintf("%v", receivedPayload["topic"])
	}
	uuid, err := exec.Command("uuidgen").Output()
	if err != nil {
		return err
	}
	if receivedPayload["id"] != nil {
		r.ID = fmt.Sprintf("%v", receivedPayload["id"])
	} else {
		r.ID = strings.TrimSuffix(string(uuid), "\n")
	}
	if receivedPayload["status"] != nil && receivedPayload["status"] != "" {
		r.Status = fmt.Sprintf("%v", receivedPayload["status"])
	} else {
		r.Status = "pending"
	}
	if receivedPayload["retries"] == nil {
		r.Retries = 0
	}
	if receivedPayload["datecreated"] != nil && receivedPayload["datecreated"] != "" {
		r.DateCreated, err = time.Parse(time.RFC3339, receivedPayload["datecreated"].(string))
		if err != nil {
			return err
		}
	} else {
		r.DateCreated = time.Now()
	}
	r.Data = receivedPayload["data"]
	r.DateRetried = time.Now()
	return nil
}
