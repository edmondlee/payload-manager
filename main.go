package main

import (
	"log"
	"manager/api/manager/queue"
	"manager/api/routes"
	"net/http"
)

func main() {
	go queue.CronRetryExpiredPayloads(86400)
	router := routes.NewRouter()
	log.Fatal(http.ListenAndServe(":8000", router))
}
